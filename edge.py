import pygame
from node import Node, Protein, TF, Kinase
import pygame

class Edge():
    def __init__(self, node1, node2):
        self.node1 = node1
        self.node2 = node2
        self.color = pygame.Color("black")
        self.state = "inactive"
        self.width = 2
        self.update_state()

    def update_state(self):
        # Déterminer si les nœuds sont des protéines ou des TF
        node1_is_protein = isinstance(self.node1, Protein)
        node2_is_protein = isinstance(self.node2, Protein)
        node1_is_TF = isinstance(self.node1, TF)
        node2_is_TF = isinstance(self.node2, TF)
        node1_is_kinase = isinstance(self.node1, Kinase)
        node2_is_kinase = isinstance(self.node2, Kinase)    

        # Cas où les deux nœuds sont des protéines
        if node1_is_protein and node2_is_protein:
            if self.node1.active and self.node2.active:
                self.state = 'active_ppi'
                self.color = pygame.Color("green")
            else:
                self.state = 'unactive_ppi'
                self.color = pygame.Color("black")
        # Cas où l'un des nœuds est un TF
        elif node1_is_TF or node2_is_TF:
            tf_node = self.node1 if node1_is_TF else self.node2
            if tf_node.active:
                self.state = 'active_tf'
                self.color = pygame.Color("orange")
            else:
                self.state = 'inactive_tf'
                self.color = pygame.Color("black")

        elif node1_is_kinase or node2_is_kinase:
            kinase_node = self.node1 if node1_is_kinase else self.node2
            protein_node = self.node2 if node1_is_kinase else self.node1
            if kinase_node.expressed:
                if protein_node.expressed:
                    if protein_node.active:
                        self.state = 'kinase_active_edge'
                        self.color = pygame.Color("green")  # Choisissez une couleur appropriée
                else:
                    self.state = 'kinase_inactive_edge'
                    self.color = pygame.Color("black")
            else:
                self.state = 'inactive'
                self.color = pygame.Color("black")

        else:
            self.state = 'inactive'
            self.color = pygame.Color("black")

    def process(self, surface):
        pygame.draw.line(surface, self.color, (self.node1.x, self.node1.y), (self.node2.x, self.node2.y), self.width)

class CellLink(Edge):
    def __init__(self, node1, node2):
        super().__init__(node1,node2)
        self.color = (255,255,255,128)
        self.width = 0
        self.update_state()

    def update_state(self):
        if self.node1.active and self.node2.active:
            self.color = pygame.Color("green")
            self.width = 10
        else:
            self.color = (255,255,255,128)
            self.width = 0
