import pygame
import random

class Node():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.radius = 10
        self.state = "inactive"
        self.present = False
        self.mutation = "wild type"
        self.color = self.orig_color = pygame.Color("black")
        self.rect = pygame.Rect(self.x - self.radius, self.y - self.radius, self.radius * 2, self.radius * 2)
        self.edges = []

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def process(self, screen):
        self.update_color()
        shape = self.draw_shape(screen)
        if self.mutation == 'mutated':
            pygame.draw.rect(screen, pygame.Color("red"), shape, 2)  # Draw red border for mutated nodes

    def update_color(self):
        if not self.present:
            self.color = pygame.Color("black")
        elif self.state == "inactive":
            self.color = pygame.Color("green")
        elif self.state == "active":
            self.color = pygame.Color("orange")

    def draw_shape(self, screen):
        raise NotImplementedError("This method should be implemented by subclasses")

    def activate(self, visited=None):
        if self.present:
            self.state = 'active'
            self.update_edges()

    def deactivate(self, visited=None):
        if self.present:
            self.state = 'inactive'
            self.update_edges()

    def mutate(self):
        self.mutation = 'mutated'
        for edge in self.edges:
            if random.random() < 0.3:
                edge.state = "permanent_deactivation"
        self.update_edges()

    def toggle_state(self):
        if self.present:
            if self.state == 'inactive':
                self.activate()
            else:
                self.deactivate()

    def toggle_mutation(self):
        if self.mutation == 'wild type':
            self.mutate()
        self.update_edges()

    def toggle_presence(self):
        self.present = not self.present
        if not self.present:
            self.deactivate()  # Ensure it gets deactivated if not present
        self.update_edges()

    def update_edges(self):
        for edge in self.edges:
            edge.update_state()

class Protein(Node):
    def __init__(self, x, y, block):
        super().__init__(x, y)
        self.block = block
        self.color = self.orig_color = self.get_color_by_block(block)
    
    def get_color_by_block(self, block):
        block_colors = {
            0: pygame.Color("orange"),
            1: pygame.Color("purple"),
            2: pygame.Color("pink")
        }
        return block_colors.get(block, pygame.Color("gray"))

    def draw_shape(self, screen):
        return pygame.draw.circle(screen, self.color, (self.x, self.y), self.radius, 0)
        

class TF(Node):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.block = None
        self.color = self.orig_color = pygame.Color("green")
    
    def draw_shape(self, screen):
        return pygame.draw.rect(screen, self.color, self.rect)

    def activate(self, visited=None):
        if visited is None:
            visited = set()

        if self in visited or not self.present:
            return

        visited.add(self)
        super().activate()
        for edge in self.edges:
            if edge.state != "permanent_deactivation":
                neighbor = edge.node1 if edge.node2 is self else edge.node2
                if isinstance(neighbor, Protein):
                    neighbor.present = True  # Make neighbor proteins present
                    neighbor.activate(visited)

    def deactivate(self, visited=None):
        if visited is None:
            visited = set()

        if self in visited or not self.present:
            return

        visited.add(self)
        super().deactivate()
        for edge in self.edges:
            if edge.state != "permanent_deactivation":
                neighbor = edge.node1 if edge.node2 is self else edge.node2
                if isinstance(neighbor, Protein):
                    neighbor.deactivate(visited)

    def toggle_presence(self):
        super().toggle_presence()
        for edge in self.edges:
            if edge.state != "permanent_deactivation":
                neighbor = edge.node1 if edge.node2 is self else edge.node2
                if isinstance(neighbor, Protein):
                    neighbor.present = self.present  # Sync presence with TF
                    neighbor.update_color()  # Update color immediately
