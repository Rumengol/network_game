import pygame, sys
from edge import Edge
from node import Node, Protein, TF
from ui.button import Button
import numpy as np
from network_generator import generate_sbm_graph, add_tf_nodes
import networkx as nx
import matplotlib.pyplot as plt
import time

# Parameters
size = width, height = 800, 800
n_clusters = 3
cluster_sizes = [10, 12, 15]  # Sizes of the clusters
intra_prob = 0.4  # Probability of intra-cluster edges
inter_prob = 0.01  # Probability of inter-cluster edges
n_tf = 3  # Number of TF nodes
tf_probs = [
    [0.4, 0.01, 0],  # Probabilities for TF1
    [0.01, 0.4, 0.02],  # Probabilities for TF2
    [0.04, 0.04, 0.4],  # Probabilities for TF3
]

# Generate the base SBM graph
graph = generate_sbm_graph(n_clusters, cluster_sizes, intra_prob, inter_prob)
graph = add_tf_nodes(graph, n_clusters, cluster_sizes, n_tf, tf_probs)
coords = nx.spring_layout(graph)

values = np.array(list(coords.values()))
min_vals = values.min(axis=0)
max_vals = values.max(axis=0)
new_min, new_max = 20, width - 20

normalized_values = (values - min_vals) / (max_vals - min_vals)
scaled_values = normalized_values * (new_max - new_min) + new_min

scaled_coords = {key: scaled_values[i] for i, key in enumerate(coords)}

pygame.init()

screen = pygame.display.set_mode(size)

init_coords = scaled_coords
init_edges = graph.edges

objects = {"edges": [], "nodes": [], "ui": []}
selected_node = None
font = pygame.font.SysFont(None, 24)
nodetype = None

# Initialize the double-click timer
last_click_time = 0
double_click_interval = 0.5  # Half a second for double-click detection
last_right_click_time = 0

def toggle_pick(args):
    global nodetype
    nodetype = args["nodetype"]

def init():
    screen.fill("white")
    for node, coords in scaled_coords.items():
        node_type = graph.nodes[node]['type']
        node_block = graph.nodes[node]['block']
        if node_type == "protein":
            node = Protein(coords[0], coords[1], block=node_block)
        if node_type == "TF":
            node = TF(coords[0], coords[1])
        objects["nodes"].append(node)
    for relation in init_edges:
        node1 = objects["nodes"][relation[0]]
        node2 = objects["nodes"][relation[1]]
        edge = Edge(node1, node2)
        objects["edges"].append(edge)
    protb = Button(480, 20, 110, 30, font, "Protein", onclickFunction=toggle_pick, onePress=False, nodetype="protein")
    objects["ui"].append(protb)
    tfb = Button(480, 60, 110, 30, font, "TF", onclickFunction=toggle_pick, onePress=False, nodetype="TF")
    objects["ui"].append(tfb)

init()

def update():
    screen.fill("white")
    for category in objects:
        for obj in objects[category]:
            obj.process(screen)

while True:
    update()
    for event in pygame.event.get():
        match event.type:
            case pygame.QUIT:
                sys.exit()
            case pygame.MOUSEBUTTONUP:
                pos = event.pos
                button = event.button
                current_time = time.time()

                if button == 1:
                    # Check for double-click to toggle presence or state
                    if current_time - last_click_time < double_click_interval:
                        for node in objects["nodes"]:
                            if node.rect.collidepoint(pos):
                                if isinstance(node, TF):
                                    node.toggle_presence()
                                    # Update presence of neighboring proteins
                                    for edge in node.edges:
                                        if edge.state != "permanent_deactivation":
                                            neighbor = edge.node1 if edge.node2 is node else edge.node2
                                            if isinstance(neighbor, Protein):
                                                neighbor.present = node.present
                                                neighbor.update_color()
                                elif isinstance(node, Protein):
                                    if node.present:
                                        node.toggle_state()
                                    else:
                                        node.toggle_presence()
                                break
                    last_click_time = current_time

                if button == 3:
                    # Check for double right-click to toggle mutation
                    if current_time - last_right_click_time < double_click_interval:
                        for node in objects["nodes"]:
                            if node.rect.collidepoint(pos):
                                node.toggle_mutation()
                                break
                    last_right_click_time = current_time

                if button == 2 and nodetype is not None:
                    # Create new node based on the selected nodetype
                    if not any(node.rect.collidepoint(pos) for node in objects["nodes"]):
                        node = Protein(pos[0], pos[1]) if nodetype == "protein" else TF(pos[0], pos[1])
                        objects["nodes"].append(node)
                        nodetype = None  # Reset nodetype after creating a node

    pygame.display.update()
