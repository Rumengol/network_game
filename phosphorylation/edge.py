import pygame

class Edge():
    def __init__(self, node1, node2):
        self.node1 = node1
        self.node2 = node2
        self.state = "normal"
        self.update_state()
        node1.edges.append(self)
        node2.edges.append(self)

    def update_state(self):
        if self.state == "permanent_deactivation":
            self.color = pygame.Color("red")
        elif self.node1.state == 'active' and self.node2.state == 'active':
            self.state = "active"
            self.color = pygame.Color("yellow")
        else:
            self.state = "inactive"
            self.color = pygame.Color("black")

    def process(self, screen):
        pygame.draw.line(screen, self.color, (self.node1.x, self.node1.y), (self.node2.x, self.node2.y), 2)
