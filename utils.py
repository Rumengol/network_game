import numpy as np
from pygame import Rect, Surface

def width_percent(surface, percentage):
    return percentage * surface.get_width() // 100

def height_percent(surface, percentage):
    return percentage * surface.get_height() // 100

def scale_values_on_surface(values, surface, margin = 60):
    values_x = values[:, 0]  # All x values
    min_vals_x, max_vals_x = values_x.min(axis=0), values_x.max(axis=0)
    values_y = values[:, 1]  # All y values
    min_vals_y, max_vals_y = values_y.min(axis=0), values_y.max(axis=0)

    # Get the width and height constraints
    new_min_x, new_max_x = margin, surface.get_rect().width - margin
    new_min_y, new_max_y = margin, surface.get_rect().height - margin

    # Normalize the values based on the minimum and maximum values in the original data
    # Assuming `values_x` and `values_y` are arrays representing the x and y coordinates
    normalized_values_x = (values_x - min_vals_x) / (max_vals_x - min_vals_x)
    normalized_values_y = (values_y - min_vals_y) / (max_vals_y - min_vals_y)

    # Scale the normalized values to fit inside the bounding box (new_min_x, new_max_x) for width
    scaled_values_x = normalized_values_x * (new_max_x - new_min_x) + new_min_x

    # Scale the normalized values to fit inside the bounding box (new_min_y, new_max_y) for height
    scaled_values_y = normalized_values_y * (new_max_y - new_min_y) + new_min_y
    scaled_values = np.column_stack((scaled_values_x, scaled_values_y))
    return scaled_values

# Adapted from https://www.pygame.org/wiki/TextWrap
def drawText(surface, text, coords, color, max_width, font, fontsize=10):

    y = coords[1]
    lineSpacing = 2
    full_rect = Rect(0,0,0,0)

    if not isinstance(text,list):
        text = [text]

    for line in text:
        while line:
            i = 1

            # determine maximum width of line
            while font.get_rect(line[:i], size= fontsize).width < max_width and i < len(line):
                i += 1

            # if we've wrapped the line, then adjust the wrap to the last word      
            if i < len(line): 
                i = line.rfind(" ", 0, i) + 1
                if i == 0:
                    print("Font size too big to be drawn on this surface")
                    return full_rect

            # render the font on the surface
            text_rect = font.render_to(surface, (coords[0], y), line[:i], color, size = fontsize)

            full_rect.width = max(full_rect.width, text_rect.width)
            full_rect.height += text_rect.height + lineSpacing

            y += fontsize + lineSpacing

            # remove the line we just blitted
            line = line[i:]

    return full_rect

def center_text_on(surface, text, font, fontsize, color, x = -1, y = -1):
    """Center text on surface horizontally, vertically or both.
    
    By default, center on both axes. If x or y is given, will not center on this axis.
    Simply renders the text to given coordinates if both axes are set."""
    tmp_surf = Surface((1,1))
    font_rect = font.render_to(tmp_surf, (0,0), text, color, size = fontsize)

    if x == -1:
        x = surface.get_width()//2 - font_rect.width//2
    if y == -1:
        y = surface.get_height()//2 - font_rect.height//2

    font.render_to(surface, (x,y), text, color, size = fontsize)
