import pygame
import json
import random
import numpy as np

# Définir une seed pour générer des nombres aléatoires
random.seed(43)

# Charger les fichiers de coordonnées des noeuds et des arêtes
with open('flattened_node_coordinates.json', 'r') as file:
    all_nodes = json.load(file)

with open('flattened_edge_connections.json', 'r') as file:
    all_edges = json.load(file)

# values = np.array(list(all_nodes.values()))

# Initialiser Pygame
pygame.init()

# Paramètres de la fenêtre
screen = pygame.display.set_mode((1000, 800))
pygame.display.set_caption("")
screen.fill((255, 255, 255))

class InitNode():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.radius = 5
        self.color = pygame.Color("blue")
        self.edges = []
        self.active = False
        self.solver_name = ""

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (int(self.x), int(self.y)), self.radius, 0)

    def set_active(self, active):
        self.active = active
        self.color = pygame.Color("black") if active else pygame.Color("blue")
        # Mettre à jour les couleurs des arêtes connectées
        for edge in self.edges:
            edge.update_color()

    def set_solver_name(self, name):
        self.solver_name = name
        print(f"Solver name set for node at ({self.x}, {self.y}): {self.solver_name}")

class InitEdge():
    def __init__(self, node1, node2):
        self.node1 = node1
        self.node2 = node2
        self.color = pygame.Color("white")
        self.node1.edges.append(self)
        self.node2.edges.append(self)
        self.update_color()

    def update_color(self):
        if self.node1.active and self.node2.active:
            self.color = pygame.Color("black")
        else:
            self.color = pygame.Color("red")

    def draw(self, screen):
        pygame.draw.line(screen, self.color, (int(self.node1.x), int(self.node1.y)), (int(self.node2.x), int(self.node2.y)), 2)

# Créer des nœuds et des arêtes à partir des données chargées
nodes = {}
edges = []

# Créer les nœuds
for node_id, coords in all_nodes.items():
    node = InitNode(*coords)
    nodes[node_id] = node

# Créer les arêtes
for edge in all_edges:
    node1 = nodes[edge[0]]
    node2 = nodes[edge[1]]
    edge_obj = InitEdge(node1, node2)
    edges.append(edge_obj)

# Boucle de jeu principale
game_running = True
input_active_node = None
input_text = ""

while game_running:
    screen.fill((255, 255, 255))

    # Dessiner toutes les arêtes
    for edge in edges:
        edge.draw(screen)

    # Dessiner tous les nœuds
    for node in nodes.values():
        node.draw(screen)

    pygame.display.flip()

    # Gestion des événements
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Activer un nœud lorsqu'il est cliqué
            mouse_x, mouse_y = event.pos
            for node in nodes.values():
                if np.sqrt((node.x - mouse_x) ** 2 + (node.y - mouse_y) ** 2) <= node.radius:
                    node.set_active(True)
                    input_active_node = node
                    input_text = ""
        elif event.type == pygame.KEYDOWN and input_active_node is not None:
            if event.key == pygame.K_RETURN:
                input_active_node.set_solver_name(input_text)
                input_active_node = None
            elif event.key == pygame.K_BACKSPACE:
                input_text = input_text[:-1]
            else:
                input_text += event.unicode

pygame.quit()