import pygame
from utils import drawText, center_text_on

class Button():
    def __init__(self, x, y, width, height, font, buttonText='Button', fontsize = 10, tooltip = "This must do something...", onclickFunction=None, onePress=False, centre=True, border_radius = 0, active = False, **kwargs):
        self.x = x - width/2 if centre else x 
        self.y = y - height/2 if centre else y
        self.width = width
        self.height = height
        self.onclickFunction = onclickFunction
        self.onePress = onePress
        self.font = font
        self.fontsize = fontsize
        self.text = buttonText
        self.tooltip = tooltip
        self.border_radius = border_radius
        self.active = active
        self.kwargs = kwargs

        self.fillColors = {
            'normal': '#ffffff',
            'hover': '#666666',
            'pressed': '#333333',
            'active': pygame.Color("green")
        }

        self.buttonSurface = pygame.Surface((self.width, self.height))
        self.buttonRect = pygame.Rect(self.x, self.y, self.width, self.height)

        self.alreadyPressed = False
        self.time_hovering = 0
        self.tooltip_max_width = 300
        self.hovering_thresh = 24

    def process(self, screen):

        mousePos = pygame.mouse.get_pos()

        self.buttonSurface.fill(self.fillColors['normal'] if not self.active else self.fillColors['active'])

        if self.buttonRect.collidepoint(mousePos):
            self.buttonSurface.fill(self.fillColors['hover'])
            self.time_hovering += 1

            if self.time_hovering >= self.hovering_thresh:
                tt_rect = drawText(self.buttonSurface, self.tooltip, (self.buttonSurface.get_width(), self.buttonSurface.get_height()),(20,20,20), self.tooltip_max_width, self.font, self.fontsize//1.5)
                tooltip_surf = pygame.Surface((min(tt_rect.width, self.tooltip_max_width) + 8, tt_rect.height + 8))
                tooltip_surf.fill((245,245,220))
                drawText(tooltip_surf, self.tooltip, (4,4), (20,20,20), self.tooltip_max_width, self.font, self.fontsize//1.5)

                # Display the tooltip over the cursor.
                # If the tooltip will appear outside of the screen, display it under the cursor instead
                tty = mousePos[1] - tooltip_surf.get_height() if mousePos[1] - tooltip_surf.get_height() >= 0 else mousePos[1]
                # Same logic, but for the right hand size.
                # If the tooltip is supposed to appear below the cursor, also flip to the left of the cursor
                ttx = mousePos[0] if mousePos[0] + tooltip_surf.get_width() <= screen.get_width() or tty == mousePos[1] else mousePos[0] - tooltip_surf.get_width()

            if pygame.mouse.get_pressed(num_buttons=3)[0]:
                self.buttonSurface.fill(self.fillColors['pressed'])

                if self.onePress:
                    self.onclickFunction(self.kwargs, self)

                elif not self.alreadyPressed:
                    self.onclickFunction(self.kwargs, self)
                    self.alreadyPressed = True

            else:
                self.alreadyPressed = False
        else:
            self.time_hovering = 0

        center_text_on(self.buttonSurface, self.text, self.font, self.fontsize, (20,20,20))

        screen.blit(self.buttonSurface, self.buttonRect)

        if self.time_hovering >= self.hovering_thresh:
            screen.blit(tooltip_surf, (ttx, tty))
