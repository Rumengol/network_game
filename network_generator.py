import numpy as np
import networkx as nx

def generate_sbm_graph(n_clusters, cluster_sizes, intra_prob, inter_prob):
    sizes = cluster_sizes
    n = sum(sizes)
    
    # Create the probability matrix for the SBM
    p_matrix = np.full((n_clusters, n_clusters), inter_prob)
    np.fill_diagonal(p_matrix, intra_prob)
    
    # Generate the graph using NetworkX
    graph = nx.stochastic_block_model(sizes, p_matrix)
    nx.set_node_attributes(graph, 'protein', 'type')

    return graph

def add_tf_nodes(graph, n_clusters, cluster_sizes, n_tf, tf_probs):
    """
    Add TF nodes to the graph with specified connection probabilities to clusters,
    and associate each TF with the block (cluster) it has the most connections to.
    """
    n = len(graph.nodes())
    current_node_id = n
    cluster_indices = np.cumsum([0] + cluster_sizes)

    for tf_index in range(n_tf):
        graph.add_node(current_node_id, type='TF', block=None)
        tf_connections = {cluster_index: 0 for cluster_index in range(n_clusters)}

        for cluster_index in range(n_clusters):
            start_idx = cluster_indices[cluster_index]
            end_idx = cluster_indices[cluster_index + 1]
            cluster_nodes = range(start_idx, end_idx)
            prob = tf_probs[tf_index][cluster_index]
            for node in cluster_nodes:
                if np.random.rand() < prob:
                    graph.add_edge(current_node_id, node)
                    tf_connections[cluster_index] += 1  # Mettez à jour les connexions

        # Attribuer le TF au bloc avec lequel il a le plus de connexions
        max_cluster = max(tf_connections, key=tf_connections.get)
        graph.nodes[current_node_id]['block'] = max_cluster

        current_node_id += 1  # Incrémentez l'ID du nœud après avoir traité le TF

    return graph

def add_kinase_nodes(graph, n_clusters, cluster_sizes, n_kinases, kinase_probs):
    """
    Ajoute des nœuds Kinase au graphe avec des probabilités de connexion spécifiées aux clusters.
    
    :param graph: Le graphe de base
    :param n_clusters: Nombre de clusters
    :param cluster_sizes: Liste des tailles pour chaque cluster
    :param n_kinases: Nombre de nœuds Kinase à ajouter
    :param kinase_probs: Liste des listes de probabilités de connexion pour chaque nœud Kinase à chaque cluster
    :return: Graphe avec les nœuds Kinase ajoutés
    """
    n = len(graph.nodes())
    current_node_id = n
    cluster_indices = np.cumsum([0] + cluster_sizes)

    for kinase_index in range(n_kinases):
        graph.add_node(current_node_id, type='Kinase', block=None)
        kinase_connections = {cluster_index: 0 for cluster_index in range(n_clusters)}

        for cluster_index in range(n_clusters):
            start_idx = cluster_indices[cluster_index]
            end_idx = cluster_indices[cluster_index + 1]
            cluster_nodes = range(start_idx, end_idx)
            prob = kinase_probs[kinase_index][cluster_index]
            for node in cluster_nodes:
                if np.random.rand() < prob:
                    graph.add_edge(current_node_id, node)
                    kinase_connections[cluster_index] += 1

        # Associer la Kinase au bloc avec lequel elle a le plus de connexions
        max_cluster = max(kinase_connections, key=kinase_connections.get)
        graph.nodes[current_node_id]['block'] = max_cluster

        current_node_id += 1

    return graph
