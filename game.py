import pygame, sys, pygame.freetype
from edge import Edge, CellLink
from node import Protein, TF, Cell, Kinase
from ui.button import Button
import numpy as np
from network_generator import generate_sbm_graph, add_tf_nodes, add_kinase_nodes
import networkx as nx
import matplotlib.pyplot as plt
import time, copy
from enum import Enum
import random
import cv2
import json

from utils import *

# Parameters
n_clusters = 5
cluster_sizes = [10, 12, 15, 20, 12]  # Sizes of the clusters
intra_prob = 0.5  # Probability of intra-cluster edges
inter_prob = 0.01  # Probability of inter-cluster edges
n_tf = 5  # Number of TF nodes
tf_probs = [[0 for _ in range(n_clusters)] for _ in range(n_clusters)]
for i in range(n_clusters):
    tf_probs[i][i] = 1

n_kinases = 5 
kinase_probs = [[0 for _ in range(n_clusters)] for _ in range(n_clusters)]
for i in range(n_clusters):
    kinase_probs[i][i] = 1

num_mutated_tfs = 2
# num_mutated_proteins = 0
# num_expressed_tfs = 0
# num_activated_tfs = 0 
# num_expressed_proteins = 0
# num_activated_proteins = 0
num_mutated_proteins = 3
num_expressed_tfs = 2
num_activated_tfs = 1 
num_expressed_proteins = 5
num_activated_proteins = 3

n_active_blocks = 2

no_mutation_mode = True

pygame.init()
objects = {"auras": [], "edges": [], "nodes": [], "ui": [], "actions": []}

# Surfaces
screen = pygame.display.set_mode(flags=pygame.FULLSCREEN)

UI_surface = pygame.Surface((width_percent(screen,100),height_percent(screen,30)), pygame.SRCALPHA)
UI_COORDINATES = (0,0)
game_surface = pygame.Surface((width_percent(screen,50), height_percent(screen,70)), pygame.SRCALPHA)
GAME_COORDINATES = (0, height_percent(screen,30))
goal_surface = pygame.Surface((width_percent(screen,50), height_percent(screen,70)), pygame.SRCALPHA)
GOAL_COORDINATES = (width_percent(screen, 50), height_percent(screen, 30))

# Home screen init

# Charger les fichiers de coordonnées des noeuds et des arêtes
with open('flattened_node_coordinates.json', 'r') as file:
    all_nodes = json.load(file)

home_values = np.array(list(all_nodes.values()))
scaled_home_values = scale_values_on_surface(home_values, screen)
scaled_home_coords = {key: scaled_home_values[i] for i, key in enumerate(all_nodes)}

with open('flattened_edge_connections.json', 'r') as file:
    all_edges = json.load(file)

home_nodes = {}
home_edges = []

for home_id, coords in scaled_home_coords.items():
    node = Cell(*coords)
    home_nodes[home_id] = node
for edge in all_edges:
    node1 = home_nodes[edge[0]]
    node2 = home_nodes[edge[1]]
    link = CellLink(node1,node2)
    home_edges.append(link)
    node1.edges.append(link)
    node2.edges.append(link)

current_cell = None

selected_node = None
font = pygame.font.SysFont(None, 24)
scalable_font = pygame.freetype.Font(None)
nodetype = None

# Background initialisation adapted from https://stackoverflow.com/a/69054207

game_video = cv2.VideoCapture("assets/game_background.mp4") # Free video stock taken from https://mixkit.co/free-stock-video/cellular-environment-under-a-microscope-3968/
game_fps = game_video.get(cv2.CAP_PROP_FPS) # 24 FPS
game_video_reversed = False

home_video = cv2.VideoCapture("assets/home.mp4") # Free video stock taken from https://www.pexels.com/fr-fr/video/mer-nature-eau-ocean-19389720/
home_fps = home_video.get(cv2.CAP_PROP_FPS)
home_video_reversed = False
fps = 24
clock = pygame.time.Clock()

frame_counter = 0
goal_image = None

# Initialize the double-click timer
last_click_time = 0
double_click_interval = 0.5  # Half a second for double-click detection
last_right_click_time = 0

# Player variables
class ActionEnum(Enum):
    EXPRESSION = ["Expression et Traduction","Exprime puis traduit un noeud. Ce n'est possible que sur les noeuds inactifs. Une fois traduites, les protéines devront être activées pour avoir un impact."]
    INHIBITION = ["Inhibition","Inhibe l'expression d'un noeud, ce qui le désactive complètement, peu importe son état (exprimé ou activé). S'il s'agit d'un facteur de transcription, les protéines qu'il active sont également inhibées."]
    ACTIVATION = ["Phosphorylation","La phosphorylation est le processus qui permet d'activer les protéines, complétant leur processus de maturation et rendant effective."]
    DEACTIVATION = ["Déphosphorylation","Retirer la phosphorylation des protéines les rend inactives, même si elles sont toujours présentes dans la cellule"]
    GENE_THERAPY = ["Thérapie Génique","La thérapie génique est un type de traitement encore rare, permettant de réparer un gène muté directement au niveau de l'ADN"]
    # MUTATION = "Mutation"  # Removed from action buttons

current_action = None
valid_nodes = []

# Game variables
game_start = False
game_end = False
turns = 0
game_won = False
name_input_surf = None
name_input_rect = None
name_input_text = ""
name_input_active = False

def assign_random_states(nodes, num_mutated_tfs, num_mutated_proteins, num_expressed_tfs, num_activated_tfs, num_expressed_proteins, num_activated_proteins):
    """
    Assigne des états aux nœuds selon les quantités spécifiées.

    Paramètres :
        - nodes : Liste des nœuds (TFs et protéines).
        - num_mutated_tfs : Nombre de TFs à muter.
        - num_mutated_proteins : Nombre de protéines à muter.
        - num_expressed_tfs : Nombre de TFs à exprimer parmi les TFs non mutés.
        - num_activated_tfs : Nombre de TFs à activer parmi les TFs exprimés.
        - num_expressed_proteins : Nombre de protéines à exprimer parmi les protéines non mutées.
        - num_activated_proteins : Nombre de protéines à activer parmi les protéines exprimées.
    """
    # Séparer les nœuds en TFs et protéines
    tfs = [node for node in nodes if isinstance(node, TF) and not isinstance(node, Kinase)]

    kinases = [node for node in nodes if isinstance(node, Kinase)]

    proteins = [node for node in nodes if isinstance(node, Protein)]
    
    # Mélanger les listes pour randomiser la sélection
    random.shuffle(tfs)
    random.shuffle(kinases)
    random.shuffle(proteins)
    
    # Muter les TFs
    if num_mutated_tfs > len(tfs):
        raise ValueError("Pas assez de TFs pour muter.")
    mutated_tfs = tfs[:num_mutated_tfs]
    for tf in mutated_tfs:
        tf.mutate()
        # Inhiber les voisins des TFs mutés
        for edge in tf.edges:
            neighbor = edge.node1 if edge.node2 is tf else edge.node2
            neighbor.inhibit()

    # Muter les Kinases (même nombre que les TFs mutés)

    if num_mutated_tfs > len(kinases):
        raise ValueError("Pas assez de Kinases pour muter.")
    mutated_kinases = kinases[:num_mutated_tfs]
    for kinase in mutated_kinases:
        kinase.mutate()
        # Désactiver les voisins des kinases mutées
        for edge in kinase.edges:
            neighbor = edge.node1 if edge.node2 is kinase else edge.node2
            neighbor.deactivate()
    
    # TFs restants (non mutés)
    remaining_tfs = tfs[num_mutated_tfs:]
     # Kinases restantes (non mutées)
    remaining_kinases = [kinase for kinase in kinases if not kinase.mutated]

    # Vérifier que le nombre de TFs à exprimer n'excède pas le nombre de TFs restants
    if num_expressed_tfs > len(remaining_tfs):
        raise ValueError("Pas assez de TFs non mutés pour exprimer.")
    # Sélectionner les TFs à exprimer
    expressed_tfs = remaining_tfs[:num_expressed_tfs]
    for tf in expressed_tfs:
        tf.express()
    
    # Vérifier que le nombre de Kinases à exprimer n'excède pas le nombre de Kinases restantes
    if num_expressed_tfs > len(remaining_kinases):
        raise ValueError("Pas assez de Kinases non mutées pour exprimer.")
    # Sélectionner les Kinases à exprimer (même nombre que les TFs exprimés)
    # expressed_kinases = remaining_kinases[:num_expressed_tfs]
    # for kinase in expressed_kinases:
    #     kinase.express()

    # Vérifier que le nombre de TFs à activer n'excède pas le nombre de TFs exprimés
    if num_activated_tfs > len(expressed_tfs):
        raise ValueError("Pas assez de TFs exprimés pour activer.")
    # Sélectionner les TFs à activer
    activated_tfs = expressed_tfs[:num_activated_tfs]
    for tf in activated_tfs:
        tf.activate()

    # Muter les protéines
    if num_mutated_proteins > len(proteins):
        raise ValueError("Pas assez de protéines pour muter.")
    mutated_proteins = proteins[:num_mutated_proteins]
    for protein in mutated_proteins:
        protein.mutate()
    
    # Protéines restantes (non mutées)
    remaining_proteins = proteins[num_mutated_proteins:]
    
    # Vérifier que le nombre de protéines à exprimer n'excède pas le nombre de protéines restantes
    if num_expressed_proteins > len(remaining_proteins):
        raise ValueError("Pas assez de protéines non mutées pour exprimer.")
    # Sélectionner les protéines à exprimer
    expressed_proteins = remaining_proteins[:num_expressed_proteins]
    for protein in expressed_proteins:
        protein.express()
    
    # Vérifier que le nombre de protéines à activer n'excède pas le nombre de protéines exprimées
    if num_activated_proteins > len(expressed_proteins):
        raise ValueError("Pas assez de protéines exprimées pour activer.")
    # Sélectionner les protéines à activer
    activated_proteins = expressed_proteins[:num_activated_proteins]
    for protein in activated_proteins:
        protein.activate()


def generate_target_network(nodes, n_active_blocks):
    """Génère un réseau cible avec des blocs activés et inhibés, tout en laissant les nœuds mutés dans les blocs inhibés."""
    # Séparer les nœuds en blocs
    blocks = {}
    for node in nodes:
        block = node.block
        if block not in blocks:
            blocks[block] = []
        blocks[block].append(node)

    # Choisir les blocs à activer
    all_blocks = list(blocks.keys())
    random.shuffle(all_blocks)
    
    # Sélectionner les blocs pour activation
    blocks_to_activate = all_blocks[:n_active_blocks]
    blocks_to_inhibit = [block for block in all_blocks if block not in blocks_to_activate]

    # Mettre à jour les états des nœuds
    for block, block_nodes in blocks.items():
        if block in blocks_to_activate:
            # Exprimer et activer tous les nœuds de ce bloc
            for node in block_nodes:
                if node.mutated:
                    node.demutate()  # S'assurer que tous les nœuds non mutés sont démutes avant activation
                node.express()
                # Expressed nodes have 75% of chances to become activated in the target network
                if random.random() < 0.75:
                    node.activate()
        else:
            # Inhibate some nodes, dephosphorylate others
            for node in block_nodes:
                if not node.mutated:
                    # If a node is active, it has 80% of chances to be just deactivated.
                    # If a node is just expressed, it has 70% of chances to stay expressed
                    # Else in both case, it is inhibited.
                    if node.active and random.random() < 0.8:
                        node.deactivate()
                    elif node.expressed and random.random() < 0.7:
                        continue
                    else:
                        node.inhibit()

    # Mettre à jour les TFs en fonction de leur bloc
    for node in nodes:
        if isinstance(node, TF):
            if node.block in blocks_to_activate:
                # TF associé à un bloc activé
                node.express()
                node.activate()
            else:
                if node.mutated:
                    inhibit_tf = True
                    for edge in node.edges:
                        neighbor = edge.node1 if edge.node2 is node else edge.node2
                        if neighbor.block in blocks_to_activate:
                            inhibit_tf = False  # S'il a au moins un voisin activé, on ne l'inhibe pas
                            break
                    if inhibit_tf:
                        node.inhibit()
                # TF non associé à un bloc activé
                if not node.mutated:
                    node.inhibit()

    # Mettre à jour les Kinases en fonction de leur bloc
    for node in nodes:
        if isinstance(node, Kinase):
            if node.block in blocks_to_activate:
                # Kinase associée à un bloc activé
                if not node.mutated:
                    node.express()
                    node.deactivate()
                    # Les Kinases n'ont pas besoin d'être activées
            else:
                # Kinase non associée à un bloc activé
                node.inhibit()


def save_graph_as_png(nodes, edges, filename="graph.png"):
    # Déterminer la taille de la surface en fonction des coordonnées des nœuds
    padding = 50
    min_x = min(node.x for node in nodes) - padding
    max_x = max(node.x for node in nodes) + padding
    min_y = min(node.y for node in nodes) - padding
    max_y = max(node.y for node in nodes) + padding
    width = max_x - min_x
    height = max_y - min_y
    
    # Créer une surface avec le fond blanc
    surface = pygame.Surface((width, height), pygame.SRCALPHA)
    surface.fill((255,255,255,128))

    # Dessiner les arêtes en premier
    for edge in edges:
        start_pos = (edge.node1.x - min_x, edge.node1.y - min_y)
        end_pos = (edge.node2.x - min_x, edge.node2.y - min_y)
        pygame.draw.line(surface, edge.color, start_pos, end_pos, 2)

    # Dessiner les nœuds par-dessus les arêtes
    for node in nodes:
        pos = (node.x - min_x, node.y - min_y)
        if isinstance(node, TF):
            pygame.draw.rect(surface, node.color, 
                             pygame.Rect(pos[0] - node.radius, pos[1] - node.radius, 
                                         node.radius * 2, node.radius * 2))
        elif isinstance(node, Kinase):
            # Dessiner la Kinase en triangle
            points = [
                (pos[0], pos[1] - node.radius),  # Sommet supérieur
                (pos[0] - node.radius, pos[1] + node.radius),  # Coin inférieur gauche
                (pos[0] + node.radius, pos[1] + node.radius)   # Coin inférieur droit
            ]
            pygame.draw.polygon(surface, node.color, points)
        else:
            pygame.draw.circle(surface, node.color, pos, node.radius)
        
    pygame.image.save(surface, filename)
    print(f"Graphe sauvegardé dans {filename}")

def check_victory(current_nodes, target_nodes):
    for current_node, target_node in zip(current_nodes, target_nodes):
        # Vérifier si le nœud est muté dans le réseau cible
        if target_node.mutated:
            # Si le nœud est muté dans le réseau cible, il peut être démuté dans le réseau actuel,
            # mais il ne doit pas être exprimé ni actif
            if current_node.expressed or current_node.active:
                return False
        else:
            # Sinon, les états doivent correspondre
            if (current_node.expressed != target_node.expressed or
                current_node.active != target_node.active or
                current_node.mutated != target_node.mutated):
                return False
    return True

def toggle_pick(args):
    global nodetype
    nodetype = args["nodetype"]

def init():
    global turns
    global game_start
    global game_end
    global current_cell
    global current_action
    global fps
    global name_input_active
    current_cell = current_action = None
    game_start = game_end = name_input_active = False
    for category in objects:
        objects[category].clear()
    
    fps = home_fps
    turns = 0
    objects["nodes"] = list(home_nodes.values())

    for edge in all_edges:
        node1 = home_nodes[edge[0]]
        node2 = home_nodes[edge[1]]
        link = CellLink(node1,node2)
        home_edges.append(link)
        node1.edges.append(link)
        node2.edges.append(link)
    
    objects["edges"] = home_edges

    for edge in objects["edges"]:
        edge.update_state()

    objects["ui"] += [
        Button(100,50,width_percent(screen,10), height_percent(screen,5), scalable_font, "Mutations", height_percent(screen,2), "Si le bouton est vert, alors des mutations seront présentes dans les cellules", set_mutation, active=not no_mutation_mode)
    ]

def set_mutation(_, button):
    global no_mutation_mode
    button.active = not button.active
    no_mutation_mode = not no_mutation_mode

def set_valid_nodes():
    for node in objects["nodes"]:
        node.valid_target = False
        if current_action == ActionEnum.EXPRESSION:
            if not node.expressed and not node.mutated:
                node.valid_target = True
        elif current_action == ActionEnum.ACTIVATION:
            if node.expressed and not node.active and not node.mutated and not isinstance(node, Kinase):
                node.valid_target = True
        elif current_action == ActionEnum.DEACTIVATION:
            if node.active:
                node.valid_target = True
        elif current_action == ActionEnum.INHIBITION:
            if node.expressed and not node.mutated:
                node.valid_target = True
        elif current_action == ActionEnum.GENE_THERAPY:
            if node.mutated:
                node.valid_target = True

def set_action(kwargs, button):
    global current_action
    for btn in objects["actions"]:
        if isinstance(btn, Button):
            btn.active = False
    button.active = True
    current_action = kwargs["action"]
    set_valid_nodes()

def new_turn():
    global turns
    turns += 1
    set_valid_nodes()
    

def get_unique_edges_from_nodes(nodes):
    unique_edges = []
    seen_edges = set()
    
    for node in nodes:
        for edge in node.edges:
            node1_id = id(edge.node1)
            node2_id = id(edge.node2)
            edge_key = tuple(sorted([node1_id, node2_id]))
            
            if edge_key not in seen_edges:
                unique_edges.append(edge)
                seen_edges.add(edge_key)
    
    return unique_edges


def game_init():
    global game_won
    game_won = False 

    global game_start
    game_start = True

    global fps
    fps = game_fps

    for category in objects:
        objects[category].clear()

    # Graph init

    while True:
        graph = generate_sbm_graph(n_clusters, cluster_sizes, intra_prob, inter_prob)
        graph = add_tf_nodes(graph, n_clusters, cluster_sizes, n_tf, tf_probs)
        graph = add_kinase_nodes(graph, n_clusters, cluster_sizes, n_kinases, kinase_probs)

        # Check connectivity
        if nx.is_connected(graph):
            break
        else:
            print("Non connex graph... New attempt.")


    coords = nx.spring_layout(graph)

    values = np.array(list(coords.values()))

    scaled_game_values = scale_values_on_surface(values, game_surface)
    scaled_game_coords = {key: scaled_game_values[i] for i, key in enumerate(coords)}

    # scaled_goal_values = scale_values_oœ_values[i] for i, key in enumerate(coords)}

    init_coords = scaled_game_coords
    init_edges = graph.edges

    # Initialisation des nœuds
    node_list = []
    for node_idx, coords in init_coords.items():
        node_type = graph.nodes[node_idx]['type']
        node_block = graph.nodes[node_idx].get('block', None)
        if node_type == "protein":
            node = Protein(coords[0], coords[1], block=node_block)
        elif node_type == "TF":
            node = TF(coords[0], coords[1], block=node_block)
        elif node_type == "Kinase":
            node = Kinase(coords[0], coords[1], block=node_block)

        node_list.append(node)
        objects["nodes"].append(node)

    # Création d'un dictionnaire pour accéder aux nœuds par leur identifiant initial
    node_id_mapping = {idx: node for idx, node in zip(init_coords.keys(), node_list)}

    # Initialisation des arêtes
    for relation in init_edges:
        node1 = node_id_mapping[relation[0]]
        node2 = node_id_mapping[relation[1]]
        edge = Edge(node1, node2)
        node1.edges.append(edge)  # Ajouter l'arête au nœud1
        node2.edges.append(edge)  # Ajouter l'arête au nœud2
        objects["edges"].append(edge)


    if no_mutation_mode:
        mut_prot = 0
        mut_tf = 0
    else:
        mut_prot = num_mutated_proteins
        mut_tf = num_mutated_tfs
    assign_random_states(objects["nodes"], mut_prot, mut_tf, 
                         num_expressed_tfs, num_activated_tfs, 
                         num_expressed_proteins, num_activated_proteins)

    # # Mise à jour des états des arêtes après l'assignation des états aux nœuds
    # for edge in objects["edges"]:
    #     edge.update_state()

    # Nécessaire car assign_random_states ne propage pas l'activation aux voisins
    for node in objects["nodes"]:
        if isinstance(node, TF):
            if node.mutated:
                node.deactivate()
            elif node.active:
                node.activate()

    # for edge in objects["edges"]:
    #     edge.update_state()

    global new_nodes
    global goal_image

    new_nodes = copy.deepcopy(objects["nodes"])
        
    generate_target_network(new_nodes, n_active_blocks)

    new_edges = get_unique_edges_from_nodes(new_nodes)
    goal_filename = "target_network.png"

    save_graph_as_png(new_nodes, new_edges, filename=goal_filename)

    goal_image = pygame.image.load(goal_filename)


    # Init actions
    to_remove = []
    if no_mutation_mode:
        to_remove = [ActionEnum.GENE_THERAPY]
    button_width_percent = 15
    nb_btn = len(ActionEnum) - len(to_remove)
    button_width = width_percent(UI_surface,button_width_percent)
    button_height = height_percent(UI_surface,30)
    margin = width_percent(UI_surface, (100 - (button_width_percent * nb_btn)) // (nb_btn + 1))
    objects["actions"] = [
        Button(i*button_width + (i+1)*margin, height_percent(UI_surface, 30), button_width, button_height, scalable_font, action.value[0], tooltip = action.value[1], fontsize = height_percent(UI_surface, 7), onclickFunction=set_action, onePress=False, centre=False, action=action)
        for i,action in enumerate(ActionEnum) if action not in to_remove
    ][::-1] # Process the buttons in reverse order so that tooltips are rendered over the buttons



    new_turn()

win_surf = pygame.Surface((width_percent(UI_surface,70), height_percent(UI_surface,60)))
win_msg = ["Bien joué ! La cellule est guérie et maintenant active dans l'organisme.",
            "Ecrit ton nom"]
center_text_on(win_surf, win_msg[0], scalable_font, height_percent(win_surf,20), (20,20,20), y = height_percent(win_surf,5))
center_text_on(win_surf, win_msg[1], scalable_font, height_percent(win_surf,15), (20,20,20), y = height_percent(win_surf,35))

# Create an input box that is centered horizontally
name_input_surf = pygame.Surface((width_percent(win_surf,60),height_percent(win_surf,30)))

def update():
    global game_video
    global game_video_reversed
    global home_video
    global home_video_reversed
    global name_input_rect
    clock.tick(fps)
    
    if game_start:        
        game_surface.fill((255,255,255,128))
        UI_surface.fill((255,255,255,128))
        goal_surface.fill((255,255,255,128))

        success, frame = game_video.read()
        if success:
            resized_frame = cv2.resize(frame, (screen.get_width(), screen.get_height()), interpolation=cv2.INTER_AREA)
            video_surf = pygame.image.frombuffer(
                resized_frame.tobytes(), resized_frame.shape[1::-1], "BGR"
                )
            screen.blit(video_surf, (0,0))
        else:
            if game_video.get(cv2.CAP_PROP_POS_FRAMES) == 0:
                raise Exception("Video can't be read")
            # Video reversed with https://ezgif.com/reverse-video
            game_video = cv2.VideoCapture("assets/game_background_reversed.mp4") if not game_video_reversed else cv2.VideoCapture("assets/game_background.mp4") 
            game_video_reversed = not game_video_reversed

            update() #If we looped the video, restart the function
            return

        pygame.draw.rect(game_surface, pygame.Color("black"), (0,0,game_surface.get_width(), game_surface.get_height()), 3)
        # Static UI
        
        goal_surface.blit(goal_image, (0,0))
        pygame.draw.rect(goal_surface, pygame.Color("black"), (0,0,goal_surface.get_width(), goal_surface.get_height()), 3)

        # Titles
        scalable_font.render_to(UI_surface, (width_percent(UI_surface,20), height_percent(UI_surface,90)), f"Réseau perturbé (malade)", (20, 20, 20), size = height_percent(UI_surface,10))
        scalable_font.render_to(UI_surface, (width_percent(UI_surface,70), height_percent(UI_surface,90)), f"Réseau cible (sain)", (20, 20, 20), size = height_percent(UI_surface,10))

        for category in objects:
            surface = game_surface if category in ["aura","edges","nodes"] else UI_surface
            for obj in objects[category]:
                obj.process(surface)

        # Turn counter
        center_text_on(UI_surface, f"Tour {turns}", scalable_font, height_percent(UI_surface,10), (20,20,20), y = height_percent(UI_surface,2))
        #scalable_font.render_to(UI_surface, (width_percent(UI_surface,40), height_percent(UI_surface,2)), f"Tour {turns}", (20, 20, 20), size = height_percent(UI_surface,10))
        
        if game_end:
            win_surf.fill((245,245,220))
            name_input_surf.fill((255,255,255))
            center_text_on(win_surf, win_msg[0], scalable_font, height_percent(win_surf,20), (20,20,20), y = height_percent(win_surf,5))
            center_text_on(win_surf, win_msg[1], scalable_font, height_percent(win_surf,15), (20,20,20), y = height_percent(win_surf,35))
            center_text_on(name_input_surf, name_input_text, scalable_font, height_percent(win_surf,20), (20,20,20))
            tmp_rect = win_surf.blit(name_input_surf, (width_percent(win_surf,20), height_percent(win_surf,60)))
            win_rect = UI_surface.blit(win_surf, (width_percent(UI_surface,15), height_percent(UI_surface,20)))
            name_input_rect = pygame.Rect(win_rect.left + tmp_rect.left, win_rect.top + tmp_rect.top, tmp_rect.width, tmp_rect.height)

        screen.blit(UI_surface, UI_COORDINATES)
        screen.blit(game_surface, GAME_COORDINATES)
        screen.blit(goal_surface, GOAL_COORDINATES)
    else:
        success, frame = home_video.read()
        if success:
            resized_frame = cv2.resize(frame, (screen.get_width(), screen.get_height()), interpolation=cv2.INTER_AREA)
            video_surf = pygame.image.frombuffer(
                resized_frame.tobytes(), resized_frame.shape[1::-1], "BGR"
                )
            screen.blit(video_surf, (0,0))
        else:
            if home_video.get(cv2.CAP_PROP_POS_FRAMES) == 0:
                raise Exception("Video can't be read")
            # Video reversed with https://ezgif.com/reverse-video
            home_video = cv2.VideoCapture("assets/home_reversed.mp4") if not home_video_reversed else cv2.VideoCapture("assets/home.mp4") 
            home_video_reversed = not home_video_reversed

            update() #If we looped the video, restart the function
            return
        for category in objects:
            for obj in objects[category]:
                obj.process(screen)
    

init()
run = True


while run:
    update()

    for event in pygame.event.get():
        match event.type:
            case pygame.QUIT:
                run = False
                sys.exit()
            case pygame.MOUSEBUTTONUP:
                pos = event.pos
                button = event.button

                if game_start:
                    surface = UI_surface if UI_surface.get_rect().collidepoint(pos) else game_surface
                    if surface == game_surface:
                        pos = pos[0] - GAME_COORDINATES[0], pos[1] - GAME_COORDINATES[1]
                    current_time = time.time()

                if button == 1:
                    if game_end:      
                        if name_input_rect.collidepoint(pos):
                            name_input_active = True
                        else:
                            name_input_active = False
                    change = False
                    for node in objects["nodes"]:
                        if node.rect.collidepoint(pos):
                            if game_start and not game_end:
                                if node.valid_target:
                                    match current_action:
                                        case ActionEnum.EXPRESSION:
                                            change = node.express()
                                        case ActionEnum.ACTIVATION:
                                            change = node.activate()
                                        case ActionEnum.DEACTIVATION:
                                            change = node.deactivate()
                                        case ActionEnum.INHIBITION:
                                            change = node.inhibit()
                                        case ActionEnum.GENE_THERAPY:
                                            change = node.demutate()
                                        case _:
                                            pass
                                    if change:
                                        new_turn()
                                    # Vérification de la victoire
                                    if check_victory(objects['nodes'], new_nodes):
                                        game_end = True
                                        name_input_active = True
                                        
                            elif not node.expressed and not game_start:
                                game_init()
                                current_cell = node

                # if button == 3:
                #     # Vérifier le double clic droit pour basculer la mutation
                #     if current_time - last_right_click_time < double_click_interval:
                #         for node in objects["nodes"]:
                #             if node.rect.collidepoint(pos):
                #                 node.toggle_mutation()
                #                 break
                #     last_right_click_time = current_time
                #     if check_victory(objects['nodes'], new_nodes):
                #         print("WIN !")
                #         game_won = True
                #         break
                #     else:
                #         for current_node, target_node in zip(objects["nodes"], new_nodes):
                #             if (current_node.expressed != target_node.expressed or
                #                 current_node.active != target_node.active or
                #                 current_node.mutated != target_node.mutated):
                #                 print(f"Noeud {current_node.id} a un état différent.")

                #     new_turn()

                if button == 2 and game_start:
                    game_end = True
                    name_input_active = True

            case pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE and not game_start and not game_end:
                    pygame.quit()
                    sys.exit()
                if name_input_active:
                    match event.key:
                        case pygame.K_RETURN:
                            current_cell.set_metadata(name_input_text,turns)
                            current_cell.express()
                            current_cell.activate()
                            name_input_text = ""
                            init()
                        case pygame.K_BACKSPACE:
                            name_input_text = name_input_text[:-1]
                        case _:
                            name_input_text += event.unicode

    pygame.display.update()
