import pygame
import random
import uuid

import pygame.freetype
from utils import drawText, height_percent, width_percent

class Node():
    def __init__(self, x, y, block = None):
        self.id = uuid.uuid4()
        self.x = x
        self.y = y
        self.block = block
        self.radius = 10
        self.expressed = False
        self.active = False
        self.mutated = False
        self.color = pygame.Color("black")
        self.rect = pygame.Rect(self.x - self.radius, self.y - self.radius, self.radius * 2, self.radius * 2)
        self.edges = []
        self.valid_target = False

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def process(self, surface):
        if self.valid_target:
            pygame.draw.circle(surface, pygame.Color("blue"), (self.x, self.y), self.radius + 5, 0)
        pygame.draw.circle(surface, self.color, (self.x, self.y), self.radius, 0)

    def update_color(self):
        if self.mutated:
            self.color = pygame.Color("red")
        elif self.active:
            self.color = pygame.Color("green")
        elif self.expressed:
            self.color = pygame.Color("orange")
        else:
            self.color = pygame.Color("black")

    def express(self):
        if not self.mutated and not self.expressed:
            self.expressed = True
            self.update_color()
            self.update_edges()
            self.check_and_activate()
            return True
        return False

    def inhibit(self):
        if self.expressed or self.active:
            self.expressed = False
            self.active = False  # Désactive également si actif
            self.update_color()
            self.update_edges()
            return True

    def activate(self, visited=None):
        if self.expressed and not self.active and not self.mutated:
            self.active = True
            self.update_color()
            self.update_edges()
            return True

    def deactivate(self, visited=None):
        if self.active:
            self.active = False
            self.update_color()
            self.update_edges()
            return True

    def mutate(self):
        if not self.mutated:
            self.mutated = True
            self.expressed = False
            self.active = False
            self.update_color()
            self.update_edges()
            return True

    def demutate(self):
        if self.mutated:
            self.mutated = False
            self.expressed = True
            self.active = False
            self.update_color()
            self.update_edges()
            self.check_and_activate()
            return True

    def toggle_state(self):
        if self.active:
            self.deactivate()
        else:
            self.activate()

    def toggle_mutation(self):
        if self.mutated:
            self.demutate()
        else:
            self.mutate()
        self.update_edges()

    def update_edges(self):
        for edge in self.edges:
            edge.update_state()

    def check_and_activate(self):
        for edge in self.edges:
            neighbor = edge.node1 if edge.node2 is self else edge.node2
            if isinstance(neighbor, Kinase) and neighbor.expressed:
                self.activate()
                break 

class Protein(Node):
    def __init__(self, x, y, block):
        super().__init__(x, y, block)      

class TF(Node):
    def __init__(self, x, y, block):
        super().__init__(x, y, block)

    def process(self, surface):
        if self.valid_target:
            aura_rect = pygame.Rect(self.rect.x - 5, self.rect.y - 5, self.rect.width + 10, self.rect.height + 10)
            pygame.draw.rect(surface, pygame.Color("blue"), aura_rect)
        pygame.draw.rect(surface, self.color, self.rect)

    def inhibit(self):

        if not self.expressed:
            return False
        self.active = False
        self.expressed = False
        self.update_color()
        self.update_edges()
        for edge in self.edges:
            neighbor = edge.node1 if edge.node2 is self else edge.node2
            if neighbor.mutated:
                continue
            if neighbor.expressed or neighbor.active:
                neighbor.inhibit()
        return True
    
    def activate(self):
        if self.mutated or not self.expressed:
            return False
        self.active = True
        self.update_color()
        self.update_edges()

        for edge in self.edges:
            neighbor = edge.node1 if edge.node2 is self else edge.node2
            if neighbor.mutated:
                continue
            if neighbor.active:
                continue  # Ne pas changer l'état s'il est déjà actif
            if not neighbor.expressed:
                neighbor.express()
                self.check_and_activate_protein(neighbor)
        return True
    
    def deactivate(self):
        if not self.active:
            return False
        self.active = False
        self.update_color()
        self.update_edges()

        for edge in self.edges:
            neighbor = edge.node1 if edge.node2 is self else edge.node2
            if neighbor.mutated:
                continue
            if neighbor.expressed or neighbor.active:
                neighbor.inhibit()
        return True
    
    def check_and_activate_protein(self, protein):
        # Vérifie si une kinase connectée est exprimée pour activer la protéine
        for edge in protein.edges:
            neighbor = edge.node1 if edge.node2 is protein else edge.node2
            if isinstance(neighbor, Kinase) and neighbor.expressed and not protein.active:
                protein.activate()

class Kinase(Node):
    def __init__(self, x, y, block):
        super().__init__(x, y, block)

    def update_color(self):
        if self.mutated:
            self.color = pygame.Color("red")
        elif self.active:
            self.color = pygame.Color("green")
        elif self.expressed:
            self.color = pygame.Color("green")
        else:
            self.color = pygame.Color("black")

    def process(self, surface):
        if self.valid_target:
            pygame.draw.polygon(surface, pygame.Color("blue"), self.get_aura_points())
        pygame.draw.polygon(surface, self.color, self.get_triangle_points())

    def get_aura_points(self):
        # Retourne les coordonnées des coins du triangle
        size = self.radius * 2 
        points = [
            (self.x, self.y - size / 2 - 10),
            (self.x - size / 2 - 10, self.y + size / 2 + 5),
            (self.x + size / 2 + 10, self.y + size / 2 + 5)
        ]
        return points
    
    def get_triangle_points(self):
        # Retourne les coordonnées des coins du triangle
        size = self.radius * 2
        points = [
            (self.x, self.y - size / 2),
            (self.x - size / 2, self.y + size / 2),
            (self.x + size / 2, self.y + size / 2)
        ]
        return points
    
    def express(self):
        if not self.mutated:
            self.expressed = True
            # self.activate()
            self.update_color()
            self.update_edges()

            # Activer les protéines voisines exprimées
            for edge in self.edges:
                neighbor = edge.node1 if edge.node2 is self else edge.node2
                if isinstance(neighbor, Protein) and not neighbor.mutated:
                    if neighbor.expressed and not neighbor.active:
                        neighbor.activate()
            return True
        return False
    
    def demutate(self):
        if self.mutated:
            super().demutate()
            # Appeler express() pour exprimer la Kinase et mettre à jour les voisins
            self.express()
            return True
        return False


class Cell(Node):
    def __init__(self, x,y):
        super().__init__(x,y)
        self.solver_name = ""
        self.turns_taken = 0
        self.time_hovering = 0
        self.font = pygame.freetype.SysFont(None,24)
        self.fontsize = 15
        self.hovering_thresh = 10
        self.color = pygame.Color("red")

    def set_metadata(self, name, turns):
        self.solver_name = name.upper()
        self.turns_taken = turns

    def activate(self):
        if not self.active:
            self.active = True
            self.update_color()
            self.update_edges()
            return True

    def process(self, screen):
        super().process(screen)
        mousePos = pygame.mouse.get_pos()
        max_width = width_percent(screen,20)
        if self.rect.collidepoint(mousePos) and self.expressed:
            self.time_hovering += 1
            if self.time_hovering > self.hovering_thresh:
                self.fontsize = height_percent(screen,1.2)
                text = [f"{self.solver_name}",f"{self.turns_taken} tours"]
                tt_rect = drawText(screen, text, (screen.get_width(), screen.get_height()),(20,20,20), max_width, self.font, self.fontsize)
                tooltip_surf = pygame.Surface((min(tt_rect.width, max_width) + 8, tt_rect.height + 8))
                tooltip_surf.fill((245,245,220))
                drawText(tooltip_surf, text, (4,4), (20,20,20), max_width, self.font, self.fontsize)

                # Display the tooltip over the cursor.
                # If the tooltip will appear outside of the screen, display it under the cursor instead
                tty = mousePos[1] - tooltip_surf.get_height() if mousePos[1] - tooltip_surf.get_height() >= 0 else mousePos[1]
                # Same logic, but for the right hand size.
                # If the tooltip is supposed to appear below the cursor, also flip to the left of the cursor
                ttx = mousePos[0] if mousePos[0] + tooltip_surf.get_width() <= screen.get_width() or tty == mousePos[1] else mousePos[0] - tooltip_surf.get_width()
                screen.blit(tooltip_surf, (ttx, tty))
        else:
            self.time_hovering = 0
